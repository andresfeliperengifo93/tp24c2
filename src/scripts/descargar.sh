
#!/bin/bash

# Este script debe descargar una sola imagen desde internet en la carpeta actual.
# Puede recibir un argumento opcional indicando la clase de la imagen.
# El nombre del archivo deberá ser su suma de verificación y debe terminar en .jpg
# Asegúrese de devolver un valor de salida acorde a la situación.

url="https//random-image-pepebigotes.vercel.app/api/random-image"

directorio="/home/andres/tcp24c2/imagenes"

checksum=$(echo "$url" | md5sum | cut -d ' ' -f1)

nombre_archivo="$checksum.jpg"

wget -o "$nombre_archivo" "$url"

if [ $? -eq 0 ]; then
	echo "imagen descargada correctamente como $nombre_archivo"
else
	echo Descarga de imágenes de internet no implementada. && exit 1
fi 

