#!/bin/bash

# Este script simplemente debe chequear que haya conexión a internet.
# Asegúrese de retornar un valor de salida acorde a la situación.
# Puede que necesite modificar el archivo Dockerfile

# intento acceder a Google con el comando wget
wget -q --spider http://google.com

# verificar el codigo de salida si verdadero o falso

if [ $? -eq 0 ]; then
	echo "conexion a Internet: Si"
	exit 0 
else
	echo "conexion a Internet: No"
	exit 1 
fi
