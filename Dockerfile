# syntax = edrevo/dockerfile-plus
INCLUDE+ .Dockerfile.base

# instalar los programas necesesarios

#configuracion de la aplicacion
ENV TERM=xterm
ENV COLORTERM=24bit
COPY ["src/scripts/internet.sh/app/"]
WORKDIR /app
ENTRYPOINT ["/app/main.sh"]
RUN apt-get install -y --no-install-recommends apt-utils &&\apt-get install-y iputils-ping curl jp2a &&\rm-rf/var/lib/apt/lists/*
